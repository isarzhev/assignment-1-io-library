%define SYSCALL_EXIT 60
%define NULL_TERM 0
%define SYSCALL_READ 0
%define SYSCALL_WRITE 1
%define OUT 1
%define IN 0
%define LINE_FEED 0xA
%define LINE_SPACE 0x20
%define LINE_TAB 0x9

section .text

; Принимает код возврата и завершает текущий процесс
exit:

    mov rax, SYSCALL_EXIT               ; rax <- syscall number
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                        ; rax <- 0
    .count_str:

        cmp byte[rdi+rax],NULL_TERM     ; compare with 0
        je .end_count                   ; if 0 return
        inc rax                         ; increment counter
        jmp .count_str                  ; next iteration
    .end_count:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi

    call string_length                  ; rax <- string length
    pop rdi
    mov rsi, rdi                        ; rsi <- string pointer
    mov rdx, rax                        ; rdx <- string length
    mov rax, SYSCALL_WRITE              ; rax <- syscall number
    mov rdi, OUT                        ; rdi <- stdout descriptor

    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax                        ; acc <- 0
    mov rdi, LINE_FEED                  ; rdi <- new line

; Принимает код символа и выводит его в stdout
print_char:
    push rdi                            ; stack <- symbol
    mov rsi, rsp                        ; rsi <- stack pointer

    mov rdx, 1                          ; rdx <- string length
    mov rax, SYSCALL_WRITE              ; rax <- syscall number
    mov rdi, OUT                        ; rdi <- stdout descriptor
    syscall

    pop rdi                             ; fix stack pointer
    ret


; Выводит беззнаковое 8-байтовое число в десятичном формате

; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push r12                            ; save r12 (callee-saved)
    push r13                            ; save r13 (callee-saved)
    mov r13, rsp                        ; save stack pointer
    xor rax, rax                        ; rax <- 0
    mov rax, rdi                        ; rax <- input num
    mov r12, 10                         ; div 10 to get remainder
    sub rsp, 1                          ; allocate byte to start writing
    mov byte[rsp], NULL_TERM            ; null terminator
    .loop:
        xor rdx, rdx                    ; rdx <- 0
        div r12                         ; num / 10
        add rdx, '0'                    ; get ascii-code
        sub rsp, 1                      ; byte for digit
        mov [rsp], dl                   ; lower 8bit -> stack
        cmp rax,0                       ; if remainder == 0: return
        jz .end                         ; stop
        jmp .loop                       ; next


    .end:
        mov rdi, rsp                    ; fix stack pointer before function call
        call print_string               ; print string
        mov rsp, r13                    ; restore stack pointer
        pop r13                         ; restore callee-saved r13
        pop r12                         ; restore callee-saved r12
        ret

; Выводит знаковое 8-байтовое число в десятичном формате

print_int:
    xor rax, rax                        ; rax <- 0
    mov rax, rdi                        ; rax <- input num
    cmp rax, 0                          ; positive?
    jns print_uint                      ; if positive print
    neg rax                             ; |input|
    push rdi                            ; rdi -> stack
    mov rdi, '-'                        ; minus sign
    push rax                            ; rax -> stack
    call print_char                     ; print -
    pop rax
    pop rsi                             ; rax and rsi from stack
    mov rdi, rax
    jmp print_uint                      ; print num
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax                        ; rax <- 0
    push rbx                            ; rbx -> stack

    .equals:
        mov bl, byte [rsi+rax]          ; get string character
        cmp byte [rdi+rax], bl          ; compare current character
        jne .not_equals                 ; if not equals
        cmp byte [rdi+rax], NULL_TERM   ; is end string
        je .end                         ; if null jump end
        inc rax                         ; inc counter
        jmp .equals                     ; next
    .not_equals:
        xor rax, rax                    ; rax <- 0 (strings not equal)
        pop rbx                         ; restore rbx
        ret
    .end:
        mov rax, 1                      ; rax <- 1 (strings equeal)
        pop rbx                         ; restore rbx
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, SYSCALL_READ               ; rax <- syscall number
    mov rdi, IN                         ; rdi <- string descriptor
    sub rsp, 1                          ; allocate character mem

    mov byte [rsp], 0                   ; [rsp] <- 0
    mov rsi, rsp                        ; rsi <- character addr
    mov rdx, 1                          ; read size
    syscall
    mov rax, [rsp]                      ; save character
    inc rsp                             ; restore stack pointer
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax                        ; rax <- 0
    push rbx                            ; save rbx (callee-saved)
    push r12                            ; save r12 (callee-saved)
    mov r12, rsi                        ; r12 <- buffer size
    xor rbx, rbx                        ; rbx <- 0 (counter)


.first_empty_char:                      ; skip all whitespaces
    push rdi
    push rsi
    call read_char                      ; read character
    pop rsi
    pop rdi
    cmp al, LINE_SPACE                  ; whitespace?
    je .first_empty_char                ;   then skip
    cmp al, LINE_TAB                    ; tab?
    je .first_empty_char                ;   then skip
    cmp al, LINE_FEED                   ; newline?
    je .first_empty_char                ;   then skip
    cmp al, NULL_TERM                   ; null terminator?
    je .end_of_word                     ;   then return

.reading_word
    mov byte[rdi+rbx], al               ; current character -> buffer
    inc rbx                             ; increment counter
    push rdi                            ; save rdi
    push rsi
    call read_char                      ; read character

    pop rsi
    pop rdi                             ; restore rdi
    cmp al, LINE_SPACE                  ; whitespace?
    je .end_of_word                     ;   then return, word over
    cmp al, LINE_TAB                    ; tab?

    je .end_of_word                     ;   then return, word over
    cmp al, LINE_FEED                   ; newline?
    je .end_of_word                     ;   then return, word over
    cmp al, NULL_TERM                   ; null terminator?
    je .end_of_word                     ;   then return, word over
    cmp rbx, r12                        ; compare word length with buffer size
    je .end                             ;   if equal return
    jmp .reading_word                   ; next


 .end_of_word
    mov byte[rdi + rbx], NULL_TERM      ; null terminator to buffer
    mov rax, rdi                        ; rax <- buffer addr
    mov rdx, rbx                        ; rdx <- word length

    pop r12                             ; restore r12
    pop rbx                             ; restore rbx
    ret

 .end
    xor rax, rax                        ; rax <- 0, unsuccess
    pop r12                             ; restore r12
    pop rbx                             ; restore rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                        ; rax <- 0
    xor rdx, rdx                        ; rdx <- 0
    push rbx                            ; save rbx
    mov rbx, 10                         ; 10 for multiplication
    push r12                            ; r12 -> stack
    push r13                            ; r13 -> stack
    xor r12, r12                        ; r12 <- 0
    xor r13, r13                        ; r13 <- 0
.parsing:

    mov r12b, byte[rdi+r13]             ; read character
    cmp r12b, 0x30                      ; number?
    jb .end                             ;   if not return (lower bound)
    cmp r12b, 0x39                      ; number?
    ja .end                             ;   if not return (upper bound)
    mul rbx                             ; mul by 10, next digit
    sub r12b, 0x30                      ; ascii -> number
    add rax, r12                        ; rax += number
    inc r13                             ; count++
    jmp .parsing                        ; next

.end
    mov rdx, r13                        ; rdx <- number length

    pop r13                             ; restore r13
    pop r12                             ; restore r12
    pop rbx                             ; restore rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)

; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax                        ; rax <- 0
    cmp byte[rdi], '-'                  ; negative?
    jne parse_uint                      ;   if not parse as is
    inc rdi                             ; skip character
    call parse_uint                     ; parse number
    cmp rdx, 0                          ; what if '-'\0?
    je .not_number                      ;   then NaN
    neg rax                             ; negate number
    inc rdx                             ; rdx++ because of '-'
    ret

.not_number
    xor rax, rax                        ; rax <- 0
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax                        ; rax <- 0
    push rdi
    push rsi

    push rdx
    call string_length                  ; string length
    pop rdx
    pop rsi
    pop rdi
    push rbx                            ; save rbx

    push r12                            ; save r12 (callee-saved)
    mov r12, rax                        ; r12 <- string length
    xor rax, rax                        ; rax <- 0
    cmp rdx, r12                        ; compare string length to buffer size
    ja .loop                            ; copy if fits
    pop r12

    pop rbx

    ret


.loop:
    mov bl, byte [rdi+rax]              ; rbx <- character
    mov byte[rsi+rax], bl               ; buffer <- character
    cmp byte[rdi+rax], 0                ; is null terminator?
    je .end_copy                        ;   if yes, return
    inc rax                             ; count++
    jmp .loop                           ; next

.end_copy:
    mov rax, r12                        ; rax <- string length
    pop r12
    pop rbx
    ret
